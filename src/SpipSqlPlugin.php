<?php
namespace Polatouche\Plugin\SpipSql;

use Pimple\Container;
use Polatouche\Plugin\PluginProviderInterface;

class SpipSqlPlugin implements PluginProviderInterface {
	/**
	 * Registers services on the given app.
	 *
	 * @param Container $app An Application instance.
	 */
	public function register(Container $app) {

	}
}
