<?php

namespace Polatouche\Plugin\SpipSql\Command;

use Polatouche\Provider\Console\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;


class SpipSqlShowdiffCommand extends Command
{
	protected function configure() {
		$this->setName("spip:sql:showdiff")
			->setDescription("Liste les tables et champs présents mais non déclarés à SPIP de la BDD.");
	}

	protected function execute(InputInterface $input, OutputInterface $output) {
		$this->io = $this->getIO($input, $output);
		$spip = $this->getService('spip.loader');
		$spip->load();

		$this->showdiff();
	}

	/**
	 * Liste les tables et champs présents mais non déclarés
	 */
	public function showdiff() {
		$this->io->title("Liste des tables et champs non déclarés à SPIP");

		$tables = sql_alltable();
		$principales = lister_tables_principales();
		$auxiliaires = lister_tables_auxiliaires();
		$declarees = array_merge($principales, $auxiliaires);

		$this->io->text(count($declarees) . " table·s déclarée·s");
		$this->io->text(count($tables) . " table·s réelle·s");

		if (count($declarees) !== count($tables)) {
			$diff = array_diff($tables, array_keys($declarees));
			sort($diff);
			$this->io->text(count($diff) . " table·s non déclarée·s :");
			$this->io->columns($diff, 6, true);
		}

		$presentes = array_intersect_key($declarees, array_flip($tables));
		ksort($presentes);
		foreach ($presentes as $table => $desc) {
			$colonnes_declarees = $desc['field'];
			$colonnes = sql_showtable($table);
			$colonnes = $colonnes['field'];
			if (count($colonnes) !== count($colonnes_declarees)) {
				$diff = array_diff_key($colonnes, $colonnes_declarees);
				$this->io->section("Table " . $table . " : " . count($diff) . " colonne·s non déclarée·s");
				$rows = array_map(
					function($k, $v) {
						return ['column' => $k, 'description' => $v];
					},
					array_keys($diff),
					$diff
				);
				$this->io->atable($rows);
			}
		}
	}

}