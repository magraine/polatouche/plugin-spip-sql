<?php

namespace Polatouche\Plugin\SpipSql\Command;

use Polatouche\Provider\Console\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;


class SpipSqlShowbaseCommand extends Command
{
	protected function configure() {
		$this->setName("spip:sql:showbase")
			->setDescription("Liste les tables de la BDD.");
	}

	protected function execute(InputInterface $input, OutputInterface $output) {
		$this->io = $this->getIO($input, $output);
		$spip = $this->getService('spip.loader');
		$spip->load();

		$this->showbase();
	}

	/**
	 * Liste les tables SQL
	 */
	public function showbase() {
		$this->io->title("Liste des tables");
		$this->spip_showbase();
	}

	/**
	 * Liste les tables SQL en utilisant SPIP
	 */
	public function spip_showbase() {
		$this->io->section("Description SPIP");

		$tables = sql_alltable();
		$this->io->text(count($tables) . " table·s");
		sort($tables);
		$this->io->columns($tables, 6, true);
		return true;
	}
}