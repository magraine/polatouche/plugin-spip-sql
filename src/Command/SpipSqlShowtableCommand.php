<?php

namespace Polatouche\Plugin\SpipSql\Command;

use Polatouche\Provider\Console\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Output\OutputInterface;


class SpipSqlShowtableCommand extends Command
{
	protected function configure() {
		$this->setName("spip:sql:showtable")
			->setDescription("Décrit une table dans la base de données.")
			->addArgument('table', InputArgument::REQUIRED, 'Le nom de la table');
	}

	protected function execute(InputInterface $input, OutputInterface $output) {
		$this->io = $this->getIO($input, $output);
		$spip = $this->getService('spip.loader');
		$spip->load();

		$table = $input->getArgument('table');
		$this->showtable($table);
	}

	/**
	 * Décrit une table SQL Spip de diverses manières
	 * @param string $table
	 */
	public function showtable($table) {
		$this->io->title("Description de la table : " . $table);
		$this->pdo_showtable($table);
		$this->spip_showtable($table);
	}

	/**
	 * Décrit une table SQL en utilisant PDO
	 * @param string $table
	 */
	public function pdo_showtable($table) {
		$this->io->section("Description PDO");

		/** @var \PDO $pdo */
		$pdo = $this->getService('spip.sql')->getPdo();

		/** Méthode pour obtenir la liste des champs même si la table est vide */
		$query = $pdo->prepare('SELECT *, count(*) FROM `' . $table . '` LIMIT 1');
		$query->execute();
		$n = $query->columnCount() - 1; // ignorer le count.
		$metas = [];
		for ($i = 0; $i < $n; $i++) {
			$meta = $query->getColumnMeta($i);
			if ($meta) {
				// name en premier…
				$meta = ['name' => $meta['name']] + $meta;
				$meta['flags'] = implode(', ', $meta['flags']);
				$metas[] = $meta;
			}
		}
		$this->io->text(count($metas) . " colonne·s");
		$this->io->atable($metas);
	}


	/**
	 * Décrit une table SQL en utilisant PDO
	 * @param string $table
	 */
	public function spip_showtable($table) {
		$this->io->section("Description SPIP");

		$res = sql_showtable($table);
		if ($res) {
			if (!empty($res['field'])) {
				$rows = array_map(
					function($k, $v) {
						return ['column' => $k, 'description' => $v];
					},
					array_keys($res['field']),
					$res['field']
				);
				$this->io->text(count($rows) . " colonne·s");
				$this->io->atable($rows);
			}
			if (!empty($res['key'])) {
				$rows = array_map(
					function($k, $v) {
						return ['key name' => $k, 'columns' => $v];
					},
					array_keys($res['key']),
					$res['key']
				);
				$this->io->text(count($rows) . " clé·s");
				$this->io->atable($rows);
			}
		}
		return true;
	}
}
