<?php

namespace Polatouche\Plugin\SpipSql\Tools;


class Pdo {

	/**
	 * Retourne la description vue par PDO de la table indiquée
	 * @param \PDO $pdo
	 * @param string $table
	 * @return array Une ligne par colonne
	 */
	public static function getColumnsDescription(\PDO $pdo, $table) {
		/** Méthode pour obtenir la liste des champs même si la table est vide */
		$query = $pdo->prepare('SELECT *, count(*) FROM `' . $table . '` LIMIT 1');
		$query->execute();
		$n = $query->columnCount() - 1; // ignorer le count.
		$metas = [];
		for ($i = 0; $i < $n; $i++) {
			$meta = $query->getColumnMeta($i);
			if ($meta) {
				// name en premier…
				$meta = ['name' => $meta['name']] + $meta;
				$meta['flags'] = implode(', ', $meta['flags']);
				$metas[] = $meta;
			}
		}
		return $metas;
	}
}