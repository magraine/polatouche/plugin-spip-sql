# Polatouche : plugin SPIP SQL

Ajoute des commandes à Polatouche permettant des actions en relation avec la base de données

## Liste des commandes

### Décrire une table SQL

    polatouche spip:sql:showtable spip_articles

### Créer un dump SPIP

    polatouche spip:sql:dump:create
    polatouche spip:sql:dump:create --name=ma_sauvegarde
    
Ne pas intégrer la structure des tables dans le dump.
Note: par défaut SPIP l’enregistre dans la table spip_meta du dump.

    polatouche spip:sql:dump:create --no-struct

### Restaurer un dump SPIP

    polatouche spip:sql:dump:restore
    polatouche spip:sql:dump:restore --name=ma_sauvegarde

Par défaut la restauration utilise la structure des tables enregistrées 
dans spip_meta du fichier de dump. À défaut, il calcule la structure de 
la table en analysant la table du dump (donc au format sqlite).

Lorsqu’on restaure le dump d’un site qui avait une base SQLite sur un site mySQL,
il est plus prudent de créer le site vide à l’identique (installation + plugins),
puis de lancer la restauration en préférant utiliser la structure des
tables alors déjà en place sur le site (au format mysql donc).

Pour cela, on peut utiliser l’option `--keep-struct`, qui utilisera, pour les tables
déjà présentes sur le site, leur structure SQL, plutôt que celle du dump.

    polatouche spip:sql:dump:restore --keep-struct


## Migrer de sqlite à mysql

Migrer de sqlite à mysql consiste donc :

* à générer un dump sur notre site
  > `polatouche spip:sql:dump:create`
* Enregistrer la liste des plugins actifs actuellement sur le site.
* Déplacer `config/connect.php` en `config/connect.old.php`
* Réinstaller le site avec un connecteur sur une base mysql.
* Réactiver les plugins
* Lancer la restauration
  > `polatouche spip:sql:dump:restore --keep-struct`
